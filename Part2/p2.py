"""Project part 2

Nikolai Rozanov
December 2015
00831231
M3C


Get .so file using makefile
run: make python

Use gradient thresholding to process image.
Gradient is computed using grad_omp routine in fdmodule2d
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p2 -llapack
"""
from p2 import fdmodule2d as f2d
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt



def threshold(M,fac,display=False,savefig=False, numthreads=1):
    """
    Added default values for display and savefig,
    so that the User can call threshold with M and fac only

    Added assert statement so that fac is positive and a number and M of the right shape,
    this increases the robustness of the program, keeping it from crashing elsewhere.

    Added if else to check for rank of M, increases robustness, in case we only have grey M

    for loop and variable declaration depends on M, hence allowing arbitraty sizes in (2<=ndim(M)<=3)

    added numberthreads so that user can specify them(set to 1 as default for robustness)
    """
    #assert statements to increase robustness
    assert 2<=np.ndim(M)<=3, "error M has wrong dimensions"
    assert isinstance(fac,(int, float, long)), "Fac has to be a number"
    assert fac>=0, "error fac must be greater than 0"

    #getting the shape of M depending on its rank
    if np.ndim(M)==3:
        n2, n1, debth = np.shape(M) #need to swap n1 and n2
    else:
        n1, n2 = np.shape(M)

    #initialising df_max
    df_amp_max=np.zeros(debth)

    #setting module variables
    f2d.numthreads = numthreads

    f2d.n1 = n1
    f2d.n2 = n2

    f2d.dx1 = 1
    f2d.dx2 = 1

    #looping throught the colors
    for i in range(debth):
        #getting df_amp, df_amp_max
        _,_,df_amp, df_amp_max[i] = f2d.grad_omp(M[:,:,i])
        #getting the indices which satisfy the cut-off condition
        index_temp = df_amp<fac*df_amp_max[i]
        #setting these to 0
        M[index_temp,i] = 0


    if(display or savefig):
        plt.figure()
        plt.imshow(M)
        if(savefig):
            plt.savefig("p2.png")
        if(display):
            plt.show()


    return M, df_amp_max




if __name__ == '__main__':
    M=misc.face()
    print "shape(M):",np.shape(M)
    plt.figure()
    plt.imshow(M)
    #plt.savefig("original.png")
    plt.show() #may need to close figure for code to continue
    N,dfmax=threshold(M,0.2,True,True)
    #N,dfmax=threshold(M,0.2,False,True)
    plt.show() #should actually not be here as we set a display parameter
