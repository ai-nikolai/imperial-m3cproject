#!/bin/bash

echo '#README FILE' > readme.md
echo 'AUTHOR: Nikolai Rozanov' >> readme.md
echo ''>> readme.md

#creating and manipulating string variables to display .md file nicer

#for the mainfolder
STRING_MAINFOLDER="##Root_Folder\n>"$(echo $(ls -p| grep -v /))
STRING_MAINFOLDER=${STRING_MAINFOLDER//' '/\\n>} #replacing space with newline
STRING_MAINFOLDER=${STRING_MAINFOLDER//>/* } #swapping + for *
STRING_MAINFOLDER=${STRING_MAINFOLDER//_/' '} #swapping _ for ' '



#for the subfolders (depth =1 only)
STRING_SUBFOLDERS=$(echo $(ls -F */))
STRING_SUBFOLDERS=${STRING_SUBFOLDERS//' '/\\n+} #replacing space with newline
STRING_SUBFOLDERS=${STRING_SUBFOLDERS//Part/##Part} #indenting correctly
STRING_SUBFOLDERS=${STRING_SUBFOLDERS//+#/#} #removing unnecessary >
STRING_SUBFOLDERS=${STRING_SUBFOLDERS//+/* } #swapping + for *
STRING_SUBFOLDERS=${STRING_SUBFOLDERS///} #removing /

#append
echo $STRING_MAINFOLDER >> readme.md
echo '' >> readme.md
echo $STRING_SUBFOLDERS >> readme.md

echo '' >> readme.md
echo 'Last Update: ' $(date) >> readme.md
