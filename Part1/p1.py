"""
Nikolai Rozanov
December 2015
00831231
M3C


Test gradient routines in fdmodule2d
To build the .so module:
run: make python



gfortran -c fdmodule.f90 fdmodule2d.f90
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p1_3 -llapack
"""
from p1_3 import fdmodule as f1
from p1_3 import fdmodule2d as f2d
import numpy as np
import matplotlib.pyplot as plt

def test_grad1(n1,n2,numthreads):
    """call fortran test routines, test_grad,test_grad_omp,teat_grad2d
    with n2 x n1 matrices, run f2d_test_grad_omp with numthreads threads
    return errors and computation times

    """
    #------------ parameters

    avg_num = 10

    #------------ module variables
    f2d.n1=n1
    f2d.n2=n2

    #according to hw4
    f2d.dx1=1.0/(f2d.n1-1)
    f2d.dx2=1.0/(f2d.n2-1)

    f2d.numthreads = numthreads

    #-----------

    #calling the fortran routines avg_num number of times to compute averages
    for i in range(avg_num):
        time      = f2d.test_grad()[1]
        time_omp  = f2d.test_grad_omp()[1]
        time_2d   = f2d.test_grad2d()[1]

    #computing of errors
    error = f2d.test_grad()[0]
    error_omp  = f2d.test_grad_omp()[0]
    error_2d   = f2d.test_grad2d()[0]

    return error, error_omp, error_2d, time/avg_num, time_omp/avg_num, time_2d/avg_num




def test_gradN(numthreads):
    """input: number of threads used in test_grad_omp
    call test_grad1 with a range of grid sizes and
    assess speedup

    Conclusion: (Compare: part1_speedups.png)
    From the plots we can see that the 2d is significantly faster than the serial one, and faster than the parallel one until n= 1000,
    this is when parallel and 2d have approximately the same speed.

    The reason for this is that DGTSV is highly optimised and requires less floating point numbers and hence less Cache memory allowing it to operate in faster Caches.
    The serial version allocates on the other hand new floating point numbers everytime.

    The most optimal way is to combine 2d dgtsv with parallelisation, but for this one would have to know hardware specifc information and calculate, how much cache one method uses against the other.
    """
    #variables
    n1_vec = np.array([10, 20, 50, 100, 200, 400, 800, 1000])
    n2_vec = n1_vec

    n = len(n1_vec)

    #speedup variables
    speedup = np.zeros((n,n))
    speedup_omp = np.zeros((n,n))
    speedup_2d = np.zeros((n,n))

    #running test_grad for a crosssection of n1, n2 values
    for i in enumerate(n1_vec):
        for j in enumerate(n2_vec):
            #compute times
            error, error2, error3, time, time_omp, time_2d = test_grad1(i[1],j[1],numthreads)
            #compute speedup
            speedup[i[0],j[0]] = time/time_omp
            speedup_omp[i[0],j[0]] = time_2d/time_omp
            speedup_2d[i[0],j[0]] = time/time_2d

    #display results
    index_1=3
    index_2=7
    plt.figure()
    plt.plot(n1_vec, speedup[:,index_1], color=[0,0,0.4], label = ('Omp vs. normal Speedup  n2 = %d' %n2_vec[index_1]))
    plt.plot(n1_vec, speedup[:,index_2], color=[0,0,0.8], label = ('Omp vs. normal Speedup n2 = %d' %n2_vec[index_2]))
    plt.plot(n1_vec, speedup_omp[:,index_1], color=[0,0.4,0], label = ('Omp vs. 2d Speedup  n2 = %d' %n2_vec[index_1]))
    plt.plot(n1_vec, speedup_omp[:,index_2], color=[0,0.8,0], label = ('Omp vs. 2d Speedup n2 = %d' %n2_vec[index_2]))
    plt.plot(n1_vec, speedup_2d[:,index_1], color=[0.4,0,0], label = ('2d vs. normal Speedup n2 = %d' %n2_vec[index_1]))
    plt.plot(n1_vec, speedup_2d[:,index_2], color=[0.8,0,0], label = ('2d vs. normal Speedup n2 = %d' %n2_vec[index_2]))
    plt.legend(loc='best')
    plt.xlabel('n1')
    plt.ylabel('Speedups')
    plt.axis('tight')
    plt.title('Nikolai Rozanov, test_gradN')
    plt.savefig("part1_speedups.png")


    return speedup_omp, speedup_2d




if __name__ == "__main__":

    e,ep,e2d,t,tp,t2d = test_grad1(400,200,2)
    s,s2d = test_gradN(2)
    plt.show()
