#README FILE
AUTHOR: Nikolai Rozanov

##Root Folder
* helper.sh: aiding tool to generate readme.md file
* readme.md: this file

##Part1:
* fdmodule.f90: fdmodule for calculating derivatives
* fdmodule2d.f90: 2d version of fdmodule
* makefile: makefile to generate .so file
* p1.py: python script
* p1_3.so: .so file from fdmodules
* part1_speedups.png: file showing the speedups for part1
##Part2:
* fdmodule.f90: module to calculate derivatives
* fdmodule2d.f90: 2d version
* makefile: make file to generate .so files
* p2.png: figure from part 2
* p2.py: python script to solve part2
* p2.so: .so file
##Part3:
* adv.so: .so file
* adv_mpi.exe: mpi executable
* advmodule.f90: contains variables
* data_mpi.in: parameter file for ode.f90
* fdmoduleB.f90: fortran module for calculating derivatives
* fmpi.dat: output from test_mpi_ode.f90
* makefile: makefile to generate various .so .exe files
* ode.f90: main fortran module
* ode_mpi.f90: main fortran module using mpi
* p3.py: main python testing file
* test_adv.f90: the tester function, that was initially in the file advmodule.f90
* test_mpi_ode.f90: test function testing ode_mpi.f90(compiled and executed using make)
##Part4:
* adv2d.so: .so file from the fortran modules
* advmodule2d.f90: contains advection equation variables and parameters
* fdmodule2d.f90: contains 2d derivatives calculation
* fdmoduleB.f90: contains derivative calculation
* makefile: makefile to facilitate compiling .so files
* ode2d.f90: main fortran module
* part4_2d_solutions.png: figure showing the calculated solution
* p4.py: main python file
* part4_speedup.png: speedup graph, ilustrating speedup.

Last Update:  Thu 17 Dec 2015 19:28:55 CET
