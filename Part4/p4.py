"""
Nikolai Rozanov
December 2015
00831231
M3C

This script contains three functions:
1. function_analytic: calculates the analytic solution, given a gaussina initial conditions
2. advection2d: sets all module variables and plots if set by the user and returns the calculated solution, with the rk4 method and the time taken.
3. test_advection2d: call advection2d and then calculates the L1 error to the analytic solution. returns error and Time
4. method_test: calls test_advection for various values to determine rate of convergence and where speedup happens

Conclusions:
	The error has a rate of convergence of 2

	The speedup happens at around n1=n2 ~= 150(compare: part4_speedup.png)

	The function advection2d runs for all tf (compare: part4_2d_solutions.png)
"""
#Project part 4
import numpy as np
import matplotlib.pyplot as plt
import adv2d
#packages for plotting the solutiong in an appealing way
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
#
# to compile run: make python1
#gfortran -c advmodule2d.f90
#f2py -llapack -c advmodule2d.f90 fdmoduleB.f90 fdmodule2dp.f90 ode2d.f90 -m adv2d --f90flags='-fopenmp' -lgomp

def function_analytic(t,x,y,c1=1,c2=1,S=0):

	'''
	This function is to calculate the analytic solution of the 2d advection equation given a gaussian initial condition
	'''
	####################################################################
	#Calculating the directions individualy
	####################################################################
	fx_t=np.exp(-100.0*(np.mod(x-float(c1)*float(t),1)-0.5)**2)
	fy_t=np.exp(-100.0*(np.mod(y-float(c2)*float(t),1)-0.5)**2)

	#creating a meshgrid
	fx_temp, fy_temp = np.meshgrid(fx_t,fy_t)
	#returning the final output
	return (float(S)*float(t)+fx_temp *fy_temp) #creating the f0 meshgrid, the rational for creating f0 this way instead of first creating the meshgrid, is computational efficiency



def advection2d(n1=50,n2=50,c1=1,c2=1,S=0,tf=1,nt=16000,par=0,numthreads=1,contour=False, savefig=False, display=False):
	'''
	advection2d can be called specifying all the parameters of the advection equation and the number of points in each direction.
	Furthermore one can specify to run rk4 in parallel by setting par = 1.
	Also one can plot a 2d plot by setting contour = True, and safe this figure with savefig = True.
	If display is true, you see 1d solution,(i.e. for fixed x and fixed y), this remains in the code for testing purposes
	'''
	####################################################################
	#setting the module variables
	####################################################################
	adv2d.advmodule.c1_adv = float(c1)
	adv2d.advmodule.c2_adv = float(c2)
	adv2d.advmodule.s_adv = float(S)


	adv2d.fdmodule2d.n1 = n1
	adv2d.fdmodule2d.n2 = n2
	adv2d.fdmodule2d.numthreads = numthreads
	dx = 1.0/n1
	dy = 1.0/n2
	adv2d.fdmodule2d.dx1 = dx
	adv2d.fdmodule2d.dx2 = dy

	#print "TEST", c1,c2,S,tf,dx,dy
	adv2d.ode2d.par = par
	adv2d.ode2d.numthreads = numthreads

	####################################################################
	#creating x1, x2, the associated mesh grid and the initial condition
	####################################################################
	x = np.linspace(0,(n1-1)*dx,n1)
	y = np.linspace(0,(n2-1)*dy,n2)

	xx,yy = np.meshgrid(x,y) #creating a meshgrid

	#
	#calculating f0, and f_analytic
	#
	f_0 = function_analytic(0.0,x,y,c1,c2,S)
	f_analytic = function_analytic(float(tf),x,y,c1,c2,S)


	####################################################################
	#calculating the solutions using rk4 method only
	####################################################################
	f_rk4, time_rk4 = adv2d.ode2d.rk4(0.,f_0,tf/float(nt), nt)
	#f_euler = adv2d.ode2d.euler(0.,f_0,tf/float(nt), nt) #can run euler as well


	####################################################################
	#displaying
	####################################################################
	if contour:
		fig = plt.figure()

		ax2 = fig.add_subplot(111, projection='3d')
		ax2.plot_surface(xx,yy,f_rk4, rstride=1, cstride=1, cmap = cm.spectral_r)

		plt.xlabel('x, n1')
		plt.ylabel('y, n2')

		plt.title('Nikolai Rozanov, advection2d, given c1=%.3f, c2=%.3f, S=%.3f' %(c1,c2,S))

		if savefig:
			plt.savefig("part4_2d_solutions.png")

	####################################################################
	#Displaying cross sections
	####################################################################
	if display:
		plt.figure()

		plt.plot(x,f_analytic[1,:], color=(0.1,0.1,0.9),marker='o', label="analytic")
		plt.plot(y,f_analytic[:,1], color=(0.1,0.1,0.5),marker='*', label="analytic, transpose")

		plt.plot(x, f_euler[1,:], color = (0.5,0.1,0.1), ls = '-', label = 'Euler')
		plt.plot(x, f_rk4[1,:], color = (0.9,0.1,0.1), ls = '-.', label = 'Rk4')

		plt.plot(y, f_euler[:,1], color = (0.1,0.5,0.1), ls = '-', label = 'Euler, transpose')
		plt.plot(y, f_rk4[:,1], color = (0.1,0.9,0.1), ls = '-.', label = 'Rk4, transpose')

		plt.legend(loc = 'best')
		plt.xlabel('x')
		plt.ylabel('f')
		plt.grid()
		plt.title('Nikolai Rozanov, advection2d, For testing only')

	#returning
	return f_rk4,time_rk4 #, f_euler





def test_advection2d(n1,n2,par=0,numthreads=1,c1=1,c2=1,S=1,tf=1,nt=16000):
	'''
	This function returns the error and time of rk4 method.
	One can call this function just by specifying the grid size, however, one can also specify all parameters of the advection equation and whether to run in parallel.
	'''
	####################################################################
	#creating the preliminary variables
	####################################################################
	dx = 1.0/n1
	dy = 1.0/n2

	x = np.linspace(0,(n1-1)*dx,n1)
	y = np.linspace(0,(n2-1)*dy,n2)

	####################################################################
	#creating the analytic solution
	####################################################################
	f_analytic = function_analytic(float(tf),x,y,float(c1),float(c2),float(S))

	####################################################################
	#calculating errors
	####################################################################
	f_rk4, time_rk4 = advection2d(n1,n2,c1,c2,S,tf,nt,par, numthreads)
	e_rk4 = np.mean(np.abs(f_rk4-f_analytic))

	return e_rk4, time_rk4

def method_test(numthreads=2):
	'''
	This function runs test_advection for various values of n1, n2 to get the rate of convergence, and speed up
	'''
	####################################################################
	#setting up variables
	####################################################################
	n_vec = np.array([50,100,200])

	nn = len(n_vec)

	e = np.zeros(nn)
	speedup = np.zeros(nn)

	for i in range(nn):
		e[i], t1 = test_advection2d(n_vec[i], n_vec[i])
		_, t2 = test_advection2d(n_vec[i], n_vec[i],1,numthreads)
		speedup[i]=t1/t2

	m,p = np.polyfit(np.log(n_vec), np.log(e), 1) #comparing errors fo rone variable changing

	plt.figure()
	plt.loglog(n_vec, e, color = (0.5,0,0), ls='-', marker = 'o',label = 'Best Linear Fit')
	plt.loglog(n_vec, np.exp(p)*n_vec**(m), color= (0,1,0), ls ='-.', label = 'The actual errors')
	plt.legend(loc = 'best')
	plt.xlabel('n')
	plt.ylabel('error')
	plt.axis('tight')
	plt.title('Nikolai Rozanov, method_test, plotting the convergence rate')

	plt.figure()
	plt.plot(n_vec, speedup, color = (0.1,0,0), ls = '-', marker = 'o' ,label = ('Speedup numthreads =%d' %numthreads))
	plt.legend(loc = 'best')
	plt.xlabel('Value of N')
	plt.ylabel('Speedup')
	plt.title('Nikolai Rozanov, method_test, plotting speedup')
	plt.savefig("part4_speedup.png")
	return m


if __name__=='__main__':

	#setting parameters
	n1 = 50
	n2 = 75
	c1 = 0.5
	c2 = 0.5
	S  = 1
	tf = 1.21
	nt = 16000

	f,t = advection2d(n1,n2,c1,c2,S,tf,nt,0,1,True, True)

	#tests the rk4 methods, given the standard advection parameters c1=c2=tf=S=1
	m = method_test(2)
	print 'Rate of Covergence: ', m

	plt.show()


	'''

	used previously for testing and determining trends


	e, t= test_advection2d(50,100)
	eb, tb= test_advection2d(100,100)
	print 'Error: ', e
	print 'Error: 'eb
	print 'Error Ratio: ', e/eb

	e, t= test_advection2d(50,50)
	eb, tb= test_advection2d(100,100)
	print 'Error: ', e
	print 'Error: 'eb
	print 'Error Ratio: ', e/eb


	e, t= test_advection2d(400,400,0,1)
	eb, tb= test_advection2d(400,400,1,2)
	print 'Error: ',e, 'Time: ',t
	print 'Error: ',eb, 'Time: ',tb
	print 'Error Ratio: ', e/eb, 'Speedup:', t/tb
	plt.show()
	'''
