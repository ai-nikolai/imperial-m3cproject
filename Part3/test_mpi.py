"""
Nikolai Rozanov
December 2015
00831231
M3C

Python script to test ode_mpi.f90
"""

import numpy as np
import matplotlib.pyplot as plt

#loading f
f=np.loadtxt('fmpi.dat')

#creating x_vec
n=np.loadtxt('data_mpi.in')
x_vec = np.linspace(0,1-1./n[0],n[0])

S = float(n[4])
tf = float(n[2])
c = float(n[3])
#calculating the analytical solution
f_analytic = S*tf + np.sin(2.0*np.pi*(x_vec-c*tf))

error = np.mean(np.abs(f-f_analytic))
print 'Error:', error


plt.figure()
plt.plot(x_vec, f, color = (0.5,0.1,0.1), ls = '-', label = 'MPI')

plt.legend(loc = 'best')
plt.xlabel('x')
plt.ylabel('f')
plt.grid()
plt.title('Nikolai Rozanov, TEST_MPI')
plt.show()
