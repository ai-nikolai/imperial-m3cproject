!
!
!Nikolai Rozanov
!December 2015
!00831231
!M3C
!
!
!Project part 3
!module contains parameters for advection equation and main program tests Euler integration of adv. eqn.
!You may need to compile this code with gfortran -c advmodule.f90 before using f2py
module advmodule
    implicit none
    real(kind=8) :: S_adv,c_adv
    real(kind=8) :: c1_adv,c2_adv !for 2d advection eqn.

end module advmodule
!--------------------
