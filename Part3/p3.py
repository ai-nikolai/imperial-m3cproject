"""
Nikolai Rozanov
December 2015
00831231
M3C




Project part 3, solve the 1d advection equation in fortran"""
#
#
#compile using makefile
#run: make python1
#
#to compile the fortran into a runable python shared libary


import numpy as np
import matplotlib.pyplot as plt
import adv



'''
This script follow the classical layout, as in previous homeworks
'''




def advection1f(nt,tf,n,dx,c=1.0,S=0.0,display=False,numthreads=1):
    """solve advection equation, df/dt + c df/dx = S
    for x=0,dx,...,(n-1)*dx, and returns f(x,tf),fp(x,tf),
    and f4(x,tf) which are solutions obtained using the fortran
    routines ode_euler, ode_euler_omp, ode_rk4
    -    f(x,t=0) = sin(2 pi x/L), L = n*dx
    -    nt time steps are taken from 0 to tf
    -    The solutions are plotted if display is true
    """
    #setting adv module variables
    adv.advmodule.c_adv = c
    adv.advmodule.s_adv = S

    #setting fdmodule variables
    adv.fdmodule.n = n
    adv.fdmodule.dx = dx


    #creating x_vec, dt(such that dt*nt = tf), L and f_0
    x_vec = np.linspace(0,(n-1)*dx,n)
    dt = tf/float(nt)
    L = n*dx
    # f_0
    f_0 = np.sin(2*np.pi*x_vec/L)

    #calculating the solutions
    f_euler     = adv.ode.euler(0.,f_0,dt,nt)
    f_rk4       = adv.ode.rk4(0.,f_0,dt,nt)
    f_euler_omp = adv.ode.euler_omp(0.,f_0,dt,nt,numthreads)

    #displaying
    if display:
        plt.figure()

        plt.plot(x_vec, f_euler, color = (0.5,0.1,0.1), ls = '-', label = 'Euler')
        plt.plot(x_vec, f_rk4, color = (0.9,0.1,0.1), ls = '-.', label = 'Rk4')

        plt.legend(loc = 'best')
        plt.xlabel('x')
        plt.ylabel('f')
        plt.grid()
        plt.title('Nikolai Rozanov, advection1')


    #return f_euler, f_rk4 #in the non parallel case
    return f_euler, f_euler_omp, f_rk4





def test_advection1f(n):
    """compute scaled L1 errors for solutions with n points
    produced by advection1f when c=1,S=1,tf=1,nt=16000, L=1"""
    #parameters
    #nt=320000
    #tf=1.21
    nt=16000
    tf=1.21
    #n given my function
    dx=1.0/n #so that L=1
    c=1.0
    S=0.0
    #numthreads
    numthreads = 1


    #creating the x_vec
    x_vec = np.linspace(0,(n-1)*dx,n)

    #calculating the analytical solution
    f_analytic = S*tf + np.sin(2*np.pi*(x_vec-c*tf))

    #calling advection1f
    f,fp,f4 = advection1f(nt,tf,n,dx,c,S,False,numthreads)
    #f,fp = advection1f(nt,tf,n,dx,c,S,False, 1) #in the non parallel case

    #calculating the errors
    e  = np.mean(abs(f_analytic-f))
    ep = np.mean(abs(f_analytic-fp))
    e4 = sum(abs(f_analytic-f4))/float(n)

    #return e, ep #in the non parallel case
    return e,ep,e4 #errors from euler, euler_omp, rk4

"""This section is included for assessment and must be included as is in the final
file that you submit,"""
if __name__ == '__main__':

    n = 200
    nt = 320000
    tf = 1.21
    dx = 1.0/n
    f,fp,f4 = advection1f(nt,tf,n,dx,1,0,True)
    e,ep,e4 = test_advection1f(200)
    eb,epb,e4b = test_advection1f(400)
    print e,ep,e4
    print eb,epb,e4b
    print e/eb,ep/epb,e4/e4b

    plt.show()


    '''
    #testing test_advection and advection for various parameter values

    nt=16000
    tf=1
    n=200
    dx=1.0/n #so that L=1
    c=1
    S=0
    #fp, f4 = advection1f(nt,tf,n,dx,c,S, True)
    ep,e4 = test_advection1f(200)
    epb,e4b = test_advection1f(400)

    #print fp
    #print 'test:'
    #print f4
    #print 'test:'
    #print f4-fp
    print ep,e4
    print ep/epb, e4/e4b
    '''
